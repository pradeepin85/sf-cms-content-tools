//this extention created by ----> pradeepin2@gmail.com
var content;
var title;
var link;

var show=0;

var buckets=Array();

var bucketsSlug=Array();



function HideLoader(x){
    $(".extention-loader").hide(x);
}

function ShowLoader(x){
    $(".extention-loader").show(x);
}

function ShowContentCreation(x){
    $("#createContent").show(x);
}

function HideContentCreation(x){
    $("#createContent").hide(x);
}

function ShowContentSaveSuccess(x){
    $("#contentSaveSuccess").show(x);
}

function HideContentSaveSuccess(x){
    $("#contentSaveSuccess").hide(x);
}


$(window).load(function() {
    HideContentCreation(0);
    HideContentSaveSuccess(0);
    HideLoader(2000);
    ShowContentCreation(2000);
    $('.btn-create').click(function(){
        ShowLoader();
        saveImageContent(createImagePayload());
        HideContentCreation();
        HideLoader(1000)
        ShowContentSaveSuccess(1000);
    });

    //ShowContentSaveSuccess(4000);
    //HideContentCreation(4000);
});




function saveImageContent(payload){

	var action = "CREATE_DRAFT";
        var that = this;
        var oReq = new XMLHttpRequest();
        oReq.onreadystatechange = function () {
            if (this.readyState == 4) {
                HideContentCreation();
                HideLoader(1000)
                ShowContentSaveSuccess(1000);
            }
        };
        
        var method = "post";
        var url = "https://kmani-wsl.internal.salesforce.com:6101"+ "/services/data/v51.0/connect/managed-content/content-spaces/" + workspaceId + "/content-versions";
		var workspaceId = "";
		var sessionToken = "00Dxx0000006HqF!AQEAQJKpEYuVNA15zOAv.Qk5B166Jaq.tM96F73x78fcBgrYUpCJA2RAnAckf2SCJ03xXWlfxwDwJLm8eYSV3djvyUyxG6jT";
        oReq.open(method, url + "?contentSpaceId=" + workspaceId + "&actionName=" + action + "&contentTypeDevName=" + contentype);
        oReq.setRequestHeader('Authorization', 'Bearer ' + sessionToken);
        oReq.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        oReq.setRequestHeader("Access-Control-Allow-Origin", "*");
        oReq.send(JSON.stringify(payload)); 
    //$("#contentSaveSuccess").hide(x);
}

function createImagePayload() {
	
    var data = {};
    data.title = window.localStorage.getItem("title");
    data.body = window.localStorage.getItem("body");
    data.excerpt = "content-demo";
    data.image = {
        ref: "",
        url: window.localStorage.getItem("imgurl")
    };
    
    var payload = {
        "title": window.localStorage.getItem("title"),
        "body": data
    };
    var managedContentId = "";
    if (managedContentId && managedContentId !== "") {
        payload.managedContentId = managedContentId;
        payload.urlName = "content-demo-urlname";
    } else {
        payload.type = contentype;
    }
    return payload;
}

