//this extention created by ----> pradeepin2@gmail.com

$(document).ready(function(){
    $(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
    
    IntPopup();
    
    $("#loginForm").submit(function(){
        var email=document.getElementById('email').value;
        var password=document.getElementById('password').value;
        Login(email,password);
    });
    
    $(".login input").change(function(){
        AddLoginMassage('');
    })
    
    $("#try").click(function(){
        GetBukets();
    });
    
    $("#logout").click(function(){
        DeactivateLogin();
    });
    $("#contentType").change(function(){
        var contentType = $(this).val();
        chrome.storage.local.set({ 'contentType': contentType }, function () { });
    });

    $("#workspace").change(function(){
        var workspaceId = $(this).val();
        chrome.storage.local.set({ 'workspaceId': workspaceId }, function () { });
    });

    $("#contentMode").change(function(){
        var contentMode = $(this).val();
        if(contentMode === "create"){
            $("#workspacesGroup").show();
            $('#contentTypeGroup').show();
            CreateWorkSpaceOptions();
        }else{
            $("#workspacesGroup").hide();
            $("#contentTypeGroup").hide();
        }
        chrome.storage.local.set({ 'contentMode': contentMode }, function () { });
    });




});