//this extention created by ----> pradeepin2@gmail.com
var h=0;

setInterval(function(){
    
    hr=12;
    hr=Math.floor(hr/12);
    if ( hr != h){
        h=hr;
        LoginAuthrizedBackground();
    }
},5*60*1000); 

resetLoader();
SendFromBackground();
//GetSelectedText();

chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
    

    if (request && request.action === 'updateContent') {
    
        var w = 440;
        var h = 440;
        var left = (screen.width/2)-(w/2);
        var top = (screen.height/2)-(h/2); 
        var url = 'content.html';
        chrome.windows.create({'url': url, 'type': 'popup', 'width': w, 'height': h, 'left': left, 'top': top} , 
        function (win) {
            
            localStorage.setItem("id", request.content.id);
			localStorage.setItem("title", request.content.title);
			localStorage.setItem("body", request.content.body);
			localStorage.setItem("imgurl", request.content.imgurl);
            sendResponse(win);
          });
    }if (request && request.action === 'createContent') {
        var workSpaceId = request.workSpaceId;
        var w = 440;
        var h = 440;
        var left = (screen.width/2)-(w/2);
        var top = (screen.height/2)-(h/2); 
        var url = '';
        if(request.contentType === "news"){
            url = 'news.html'
        }else if(request.contentType === "cta"){
            url = 'cta.html';
        }else if(request.contentType === "blog"){
            url = 'blog.html';
        }else if(request.contentType === "article"){
            url = 'article.html';
        }else if(request.contentType === "image"){
            url = 'image.html'
        }
        chrome.windows.create({'url': url, 'type': 'popup', 'width': w, 'height': h, 'left': left, 'top': top} , 
        function (win) {
            
            localStorage.setItem("id", request.content.id);
			localStorage.setItem("title", request.content.title);
			localStorage.setItem("body", request.content.body);
            localStorage.setItem("imgurl", request.content.imgurl);
            localStorage.setItem("workspaceId", workSpaceId);
            localStorage.setItem("contentType", request.contentType);
            sendResponse(win);
          });
    }
  });