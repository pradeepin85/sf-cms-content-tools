//this extention created by ----> pradeepin2@gmail.com
var content;
var title;
var link;

var show=0;

var buckets=Array();

var spaces=Array();

var bucketsSlug=Array();


function HideLoader(x){
    $(".extention-loader").hide(x);
}

function ShowLoader(x){
    $(".extention-loader").show(x);
}

function HideButton(x){
    $(".extention-web-content-button").hide(x);
}

function ShowButton(x){
    $(".extention-web-content-button").show(x);
}

function AddLoader(){
    var loader = '<div class="extention-loader" style=" position:fixed; width:100%; height:100%; background-color:rgba(255,255,255,0.7); z-index:9999999999" >   <img src= "' + chrome.extension.getURL('../img/loader.gif')+'" style= " margin:auto; position: absolute; top: 50%; left: 50%;  transform: translate(-50%, -50%); text-align:right; " /> <br/> <h3 style= " margin:auto; position: absolute; top: 50%; left: 50%;  transform: translate(-50%, -50%);" > Please wait....we are saving the record to Salesforce CMS </h3></div >';
    $('body').before(loader);

}

function AddSaveButton(){
    
    var button='<img class="extention-web-content-button" id="extention-web-content-button" width="40px" id="logo" style="background-color:#fff; z-index:9999; cursor:pointer; position:absolute; margin: auto; top: 20px; left: 20px;" src='+chrome.extension.getURL("../img/icon.png")+' data-toggle="tooltip" data-placement="bottom" title="Save to Salesforce CMS" />'
    
    $('body').before(button);
    
    $('.extention-web-content-button').click(function(){
        chrome.storage.local.get('contentMode', function(result) {
            if(result.contentMode === "create"){
                    chrome.storage.local.get('workspaceId', function(result) {
                        var workspaceId = result.workspaceId;
                        chrome.storage.local.get('contentType', function(result) {
                            var contentType = result.contentType;
                            ShowContentCreation(workspaceId,contentType);
                        });
                    })
            }else {
                UpdateContent();
            }
          });
       // SendToBackground();
    });

}
////////////////////

var loginState=0;

function IntPopup(){
    $('.Login').hide();
    $('.Content').hide();  
    HideLoader(0);
    chrome.storage.local.get('login', function (result) {
        login = result.login;
        console.log(login);
        if(login){
            $('.Content').show();  
            //GetBukets();
            $(".save").hide(100);
            
            $("#save").click(function(){
                SaveBuket();
            });

        }
        else{
          $('.Login').show();  

        }
    });
}

function AddLoginMassage(msg){
    $("#massage").html(msg)
}

function Login(email,password){
    var http = new XMLHttpRequest();
    var url = "https://api.salesforcecms.com/v1/authenticate";
    var params = "email="+email+"&password="+password;
    http.open("POST", url, true);
    ShowLoader(100);

    //Send the proper header information along with the request
    /* http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    http.onreadystatechange = function () {//Call a function when the state changes.
       // HideLoader(100);
        if (http.readyState == 4 && http.status == 200) {
            response = JSON.parse(http.responseText);
            key=response.token;
            SaveApiKey(key,email,password);
            AddLoginMassage('');
            loginState=1;
            setTimeout(function(){
                

            },100);
            
            
        }
        else{
            if(!loginState)
                AddLoginMassage("Wrong Username or Password !!");
        }
    }
    http.send(params);*/
    ActivateLogin();
}

function LoginAuthrizedBackground(){
    chrome.storage.local.get('email', function (result) {
        email = result.email;
        chrome.storage.local.get('password', function (result) {
            password = result.password;
            var http = new XMLHttpRequest();
            var url = "https://api.salesforcecms.com/v1/authenticate";
            var params = "email="+email+"&password="+password;
            http.open("POST", url, true);
            ShowLoader(100);

            //Send the proper header information along with the request
            http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

            http.onreadystatechange = function () {//Call a function when the state changes.
              //  HideLoader(100);
                if (http.readyState == 4 && http.status == 200) {
                    SaveApiKey(key,email,password);
                    setTimeout(function(){ActivateLogin();},200);
                }
                else{
                    if(!loginState)
                        DeactivateLogin();
                }
            }
            http.send(params);
        });
    });
}

function SaveApiKey(key,email,password){
    chrome.storage.local.set({ 'key': key }, function () { });
    chrome.storage.local.set({ 'email': email }, function () { });
    chrome.storage.local.set({ 'password': password }, function () { });
}

function ActivateLogin(){

    
    chrome.storage.local.set({ 'login': 1 }, function () { location.reload();});
}

function DeactivateLogin(){
    chrome.storage.local.set({ 'login': 0 }, function () {
        chrome.storage.local.set({ 'key': '' }, function () {
            chrome.storage.local.set({ 'email': '' }, function () {
                chrome.storage.local.set({ 'password': '' }, function () {
                    chrome.storage.local.set({ 'slug': '' }, function () {location.reload(); });
                });
            });
        });
    });
    
    
    
}

function GetWorkspaces(){

    var workspaces = [
                      {id:"11111",name:'space1'}, 
                      {id:"22222",name:'space2'},
                      {id:"33333",name:'space3'},
                      {id:"44444",name:'space4'},
                    ];


    CreateWorkSpaceOptions(workspaces);
    chrome.storage.local.get('workspaces', function (result) {




        key = result.key;

        var http = new XMLHttpRequest();
        var url = "https://api.salesforcecms.com/v1/buckets";
        var params = {"title": "web-content-extention"};
        
        params = JSON.stringify(params);
        http.open("GET", url, true);
        ShowLoader(100);

        //Send the proper header information along with the request
        http.setRequestHeader('Authorization',' Bearer '+key );
        http.setRequestHeader('Content-type', 'application/json');
        
        http.onreadystatechange = function () {//Call a function when the state changes.
            HideLoader(100);
            if (http.readyState == 4 && http.status == 200) {
                response = JSON.parse(http.responseText);
               // alert(http.responseText);
                var bucketsResponse=Array();
                bucketsResponse=response.buckets;
                for (i=0;i<bucketsResponse.length;i++){
                    buckets[i]=response.buckets[i].title;
                    bucketsSlug[i]=response.buckets[i].slug;
                }
                CreateOptions(buckets,bucketsSlug)
                //alert(bucketsSlug);

            }
           /* else if(http.status == 409)//alerady created
            {
                alert(http.responseText);

//              /  ObjectType();
            }*/
            
        }
        http.send();
        
    });
}

function GetBukets(){
    chrome.storage.local.get('key', function (result) {
        key = result.key;

        var http = new XMLHttpRequest();
        var url = "https://api.salesforcecms.com/v1/buckets";
        var params = {"title": "web-content-extention"};
        
        params = JSON.stringify(params);
        http.open("GET", url, true);
        ShowLoader(100);

        //Send the proper header information along with the request
        http.setRequestHeader('Authorization',' Bearer '+key );
        http.setRequestHeader('Content-type', 'application/json');
        
        http.onreadystatechange = function () {//Call a function when the state changes.
            HideLoader(100);
            if (http.readyState == 4 && http.status == 200) {
                response = JSON.parse(http.responseText);
               // alert(http.responseText);
                var bucketsResponse=Array();
                bucketsResponse=response.buckets;
                for (i=0;i<bucketsResponse.length;i++){
                    buckets[i]=response.buckets[i].title;
                    bucketsSlug[i]=response.buckets[i].slug;
                }
                CreateOptions(buckets,bucketsSlug)
                //alert(bucketsSlug);

            }
           /* else if(http.status == 409)//alerady created
            {
                alert(http.responseText);

//              /  ObjectType();
            }*/
            
        }
        http.send();
        
    });
}


function CreateWorkSpaceOptions(){
    var workspaces = spaces;
    if(spaces.length > 0){
        ShowLoader(100);
        for(i=0;i<workspaces.length;i++){
            $('#workspace').append("<option value='"+workspaces[i].id+"' >"+workspaces[i].name+"</option>");
        }
        HideLoader(100);
    }else{
        var http = new XMLHttpRequest();
            var url = "https://kmani-wsl.internal.salesforce.com:6101"+ "/services/data/v51.0/connect/managed-content/content-spaces/authoring";
            http.open("GET", url, true);
            ShowLoader(100);    
            var sessionToken = "00Dxx0000006HqF!AQEAQJKpEYuVNA15zOAv.Qk5B166Jaq.tM96F73x78fcBgrYUpCJA2RAnAckf2SCJ03xXWlfxwDwJLm8eYSV3djvyUyxG6jT";
            //Send the proper header information along with the request
            http.setRequestHeader('Authorization','Bearer '+sessionToken );
            http.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            http.setRequestHeader("Access-Control-Allow-Origin", "*");
            
            http.onreadystatechange = function () {//Call a function when the state changes.
                HideLoader(100);
                workspaces = [
                    {id:"11111",name:'space1'}, 
                    {id:"22222",name:'space2'},
                    {id:"33333",name:'space3'},
                    {id:"44444",name:'space4'},
                  ];
                if (http.readyState == 4) {
                    response = JSON.parse(http.responseText);
                    //workspaces = response["spaces"];  
                    for(i=0;i<workspaces.length;i++){
                        $('#workspace').append("<option value='"+workspaces[i].id+"' >"+workspaces[i].name+"</option>");
                    } 
                }
            }
            http.send();
    }
}

function CreateOptions(title,slug){
    for(i=0;i<title.length;i++){
        $('#SelectSlug').append("<option value='"+slug[i]+"' >"+title[i]+"</option>");
        chrome.storage.local.get('slug', function (result) {
            slug = result.slug;
            console.log(slug);
            if(slug){
                ChangeLink();
                $('#SelectSlug').val(slug);
            }
            else{
                $('#bucket-massage').html("Select Bucket to start using the extension");
            }
            
                
            
            $('#SelectSlug').change(function(){
                    $(".save").show(100);
                   
            });
        });
    }
    
}

function SaveBuket(){
    slug=$('#SelectSlug').val();

    chrome.storage.local.set({ 'slug': slug }, function () {
        ObjectType();
        ChangeLink();
        $(".save").hide(100);
        $('#bucket-massage').html("");
        $('#bucket-massage-success').html("Bucket selected successfully");
    });

}

/*
function CreateBuket(){
    chrome.storage.local.get('key', function (result) {
        key = result.key;

        var http = new XMLHttpRequest();
        var url = "https://api.salesforcecms.com/v1/buckets";
        var params = {"title": "web-content-extention"};
        
        params = JSON.stringify(params);
        http.open("POST", url, true);
        ShowLoader(100);

        //Send the proper header information along with the request
        http.setRequestHeader('Authorization',' Bearer '+key );
        http.setRequestHeader('Content-type', 'application/json');
        
        http.onreadystatechange = function () {//Call a function when the state changes.
            HideLoader(100);
            if (http.readyState == 4 && http.status == 200) {
                //response = JSON.parse(http.responseText);
               // alert(http.responseText);
                ObjectType();

            }
            else if(http.status == 409)//alerady created
            {
                ObjectType();
            }
            
        }
        http.send(params);
        
    });
}*/

function ObjectType(){
    chrome.storage.local.get('key', function (result) {
        key = result.key;
        chrome.storage.local.get('slug', function (result) {
            slug = result.slug;

            var http = new XMLHttpRequest();
            var url = "https://api.salesforcecms.com/v1/"+slug+"/add-object-type";
            var params = {
                          "title": "Web Content",
                          "singular": "Web Content",
                          "slug": "Web Content",
                          "metafields": [
                          {
                              "type": "text",
                              "title": "title",
                              "key": "title",
                              "required": false
                           },
                           {
                              "type": "html-textarea",
                              "title": "content",
                              "key": "content",
                              "required": false
                           },
                           {
                              "type": "text",
                              "title": "link",
                              "key": "link",
                              "required": false
                            }

                          ]
                        };

            params = JSON.stringify(params);
            http.open("POST", url, true);
            ShowLoader(100);

            //Send the proper header information along with the request
            http.setRequestHeader('Authorization',' Bearer '+key );
            http.setRequestHeader('Content-type', 'application/json');

            http.onreadystatechange = function () {//Call a function when the state changes.
                HideLoader(100);
                if (http.readyState == 4 && http.status == 200) {
                    //response = JSON.parse(http.responseText);
                    //alert(http.responseText);

                }

            }
            http.send(params);
        });
        
    });
}

function CreateObject(title,content,link){
    chrome.storage.local.get('key', function (result) {
        key = result.key;
        chrome.storage.local.get('slug', function (result) {
            slug = result.slug;
            
            var http = new XMLHttpRequest();
            var url = "https://api.salesforcecms.com/v1/"+slug+"/add-object";
            var params = {
                          "title": title,
                          "type_slug": "Web Content",
                         "content":content,
                          "metafields": [
                            {
                              "type": "text",
                              "value": title,
                              "key": "title",
                            },
                            {
                              "type": "html-textarea",
                              "value": content,
                              "key": "content",

                            },
                            {
                              "type": "text",
                              "value": link,
                              "key": "link",
                            }

                          ], 
                          "options": {
                            "slug_field": false
                          }
                        };

            params = JSON.stringify(params);
            http.open("POST", url, true);
            loaderInc();

            //Send the proper header information along with the request
            http.setRequestHeader('Authorization',' Bearer '+key );
            http.setRequestHeader('Content-type', 'application/json');

            http.onreadystatechange = function () {//Call a function when the state changes.


                if (http.readyState == 4 && http.status == 200) {
                loaderDec();

                }


            }
            http.send(params);
        });
    });
}

function SendFromBackground() {

    //chrome.windows.create({url: "popup.html", type: "popup"});
    chrome.storage.local.get('params', function (result) {
        var params = result.params;
        chrome.storage.local.set({ 'params': '' }, function () {
            //setTimeout(function () { SendFromBackground(); }, 200); 
         });
        if (params) {
            console.log(params.title+params.content+params.link);
            CreateObject(params.title,params.content,params.link);
        }

    });

}

function loaderInc() {
    chrome.storage.local.get('loader', function (result) {
        var loader = result.loader;
        if (!loader)
            loader = 0;
        chrome.storage.local.set({ 'loader': ++loader  }, function () { });
    });
}

function loaderDec() {
    chrome.storage.local.get('loader', function (result) {
        var loader = result.loader;
        if (!loader)
            loader = 0;
        if (loader>0)
            loader--;

        chrome.storage.local.set({ 'loader': loader }, function () { });
    });
}

function resetLoader(){
    chrome.storage.local.set({ 'loader': 0 }, function () { });

}

function load(){
    chrome.storage.local.get('loader', function (result) {
        var loader = result.loader; 
        if (loader=='0')
            HideLoader(100);
    });
}

function GetSelectedText(){
    setInterval(function(){
       // var selected=window.getSelection();
        if (window.getSelection)
        {
            content = window.getSelection().toString();
            if (content  ){    
                ShowButton();
            }
            else
            {
                HideButton();
            }
        }
    },100);
}

function LoadButtonPosition(){
    $(document).mousemove(function(event) {
        posX = event.pageX;
        posY = event.pageY;
    });
    $(document).mouseup(function(event) {
        $('#extention-web-content-button').css('margin-left',posX);
        $('#extention-web-content-button').css('margin-top',posY);
    });
}

function _getImg(sel)
{
	try{
		var parent = sel.focusNode.parentNode.offsetParent.parentNode.parentNode;
		var url = parent.querySelector("div.js-content-image").style.getPropertyValue("background-image")
		var regExp = /\"([^)]+)\"/;
		return regExp.exec(url)[1];
	}catch(e) {
		console.log('failed to get image url');
	}
	return "";
}

function _getBody(sel)
{
	try{
		return sel.focusNode.parentNode.offsetParent.querySelector('lightning-formatted-rich-text span').innerText;
	}catch(e) {
		console.log('failed to get body');
	}
	return "";
}


function _getImgForCreate(sel)
{
	try{
		var parent = sel.focusNode.parentNode.querySelector("img");
		var url = parent.url;
		var regExp = /\"([^)]+)\"/;
		return regExp.exec(url)[1];
	}catch(e) {
		console.log('failed to get image url');
	}
	return "";
}

function _getBodyForCreate(sel)
{
	try{
		return sel.focusNode.parentNode.offsetParent.querySelector('p').innerText;
	}catch(e) {
		console.log('failed to get body');
	}
	return "";
}

function UpdateContent(){

    title= document.title;
    link=document.URL;
    
    //params={'title':title,'link':link,'content':content}

    chrome.runtime.sendMessage({
        action: 'updateContent',
        content:{
            title:document.title,
            link:document.url,
            content:content
        }
      },
      function(createdWindow) {
        console.log(createdWindow);
      });
}

function ShowContentCreation(workspaceId,contentType){

    title= document.title;
    link=document.URL;
    var sel = document.getSelection();
    //params={'title':title,'link':link,'content':content}
    chrome.runtime.sendMessage({
        action: 'createContent',
        contentType: contentType,
        workspaceId: workspaceId,
        content:{
            title:document.title,
            link:document.url,
            content:content,
            body: _getBodyForCreate(sel),
            imgurl: _getImgForCreate(sel)
        }
      },
      function(createdWindow) {
        console.log(createdWindow);
      });
}

function UpdateContent(){

    title= document.title;
    link=document.URL;
    
    //params={'title':title,'link':link,'content':content}
    var sel = document.getSelection();
    var contentId;
    try{
	    contentId = sel.focusNode.parentNode.offsetParent.getAttribute("data-contentId");
    }catch(e) {
    }
    
    chrome.runtime.sendMessage({
        action: 'updateContent',
        content:{
            title: sel.focusNode.data,
            link:document.url,
            content:content,
            id:contentId,
            body: _getBody(sel),
            imgurl: _getImg(sel)
        }
      },
      function(createdWindow) {
        console.log(createdWindow);
      });
}


function SendToBackground(){
    title= document.title;
    link=document.URL;
    
    //params={'title':title,'link':link,'content':content}
    //params = JSON.stringify(params);
    
    chrome.storage.local.set({ 'params': params }, function () { ShowLoader(100);});
    
}

function IntContentScript(){
   chrome.storage.local.get('key', function (result) {
        key = result.key;
        chrome.storage.local.get('slug', function (result) {
            slug = result.slug;

            //if (key &&slug){
                AddLoader();
                HideLoader();
                AddSaveButton();
                HideButton();
                GetSelectedText();
                LoadButtonPosition();
                //setInterval(function () {load(); }, 500);
            //}

        });
    });
 
}

function ChangeLink(){
    chrome.storage.local.get('slug', function (result) {
        slug = result.slug;
        $("#bucket-objects").attr("href","https://salesforcecms.com/"+slug+"/dashboard");
    });
}
